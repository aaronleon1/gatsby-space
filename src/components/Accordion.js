import React from 'react'

function Accordion(props) {
    return(
    <div className="tab w-full  border-t font-sans ">
		<input className="absolute opacity-0" id={`tab-single-${props.flightNumber}`} type="radio" name="tabs2"/>
		<label className="block p-5 leading-normal cursor-pointer font-semibold" for={`tab-single-${props.flightNumber}`}>{props.missionName} ({props.launchYear})</label>
		<div className="tab-content overflow-hidden border-l-2  border-red-500 leading-normal">
			<img className='w-32 h-32 rounded mx-auto mt-4 md:w-36 md:h-36' src={props.patch} alt='mission patch' />
			<h2 className='text-center pb-2'>Launch {props.flightNumber}</h2>
			<div className='w-11/12 md:w-3/4 mx-auto '>
			<p className='px-2 py-2'><span className='font-semibold'>Rocket:</span> {props.rocketName} || {props.rocketId === 'falcon9' ? (`${props.falconNineWeight} lbs`) : (`${props.falconHeavyweight} lbs`)} || {props.rocketId === 'falcon9' ? (`$${props.falconNineCost} launch cost`):(`$${props.falconHeavyCost} launch cost`)}</p>
			<p className='px-2 py-2'><span className='font-semibold'>Launch date:</span> {props.launchDate}</p>
			<p className='px-2 py-2'><span className='font-semibold'>Launch Site:</span> {props.launchSite}</p>
			<h3 className='px-2 pt-2 pb-0 mb-0 font-semibold'>Details</h3>
			<p className="px-2 pt-0 pb-0 overflow-auto">{props.details ? (props.details) : ('No details provided for this launch.')}</p>
			</div>
			<div className='w-full text-center py-2.5'><a href={props.article} target='_blank' className="px-1 py-3 underline font-semibold">Read more about it here</a> || <a href={props.video} target='_blank' className='px-1 my-3 underline font-semibold'>Watch it here</a></div>
		</div>
	</div>
    )
    
}

export default Accordion