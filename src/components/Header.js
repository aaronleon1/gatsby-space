import React from 'react'

function Header() {
    return(
    <header className='bg-white mb-1 shadow'>
        <div className='container py-2 px-4 my-0 mx-auto m-w-screen-xl'>
            <img src='https://new.americanreading.com/images/logos/arc-logo-white-tagline.svg' alt='american reading company logo' className='h-14 md:h-16 p-1 m-0 bg-red-500' />
        </div>
    </header>
    )
    
}

export default Header