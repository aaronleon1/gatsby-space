import React, {useEffect, useState} from 'react'
import axios from 'axios'
import '../styles/tailwind.css'
import Header from '../components/Header'
import Accordion from '../components/Accordion'


function IndexPage() {
	const [launches, setLaunches] = useState('')
	const [loaded, setLoaded] = useState(null)
	const [falconNineData, setFalconNineData] = useState('')
	const [falconHeavyData, setFalconHeavyData] = useState('')

	useEffect(() =>{
		{/*Function to get past 50 launches from SpaceX API */}
		async function getLaunches(){
			await axios.get('https://api.spacexdata.com/v3/launches/past?order=desc').then(
				(response) => {
					setLaunches(response)
					console.log(response)
					setLoaded(true)
				}
			)
		}
		{/*Function to get Falcon 9 data for weight and cost data purposes.
			May not be best practice to have these API calls done this way 
			but am doing so in the intrest of the 2-3 hour window.
		*/}
		async function getFalconNineData(){
			await axios.get('https://api.spacexdata.com/v3/rockets/falcon9').then(
				(response) => {
					setFalconNineData(response)
					console.log(response)
				}
			)
		}
		{/*Function to get Falcon Heavy data for weight and cost data purposes*/}
		async function getFalconHeavyData(){
			await axios.get('https://api.spacexdata.com/v3/rockets/falconheavy').then(
				(response) => {
					setFalconHeavyData(response)
					console.log(response)
				}
			)
		}
		getLaunches();
		getFalconNineData();
		getFalconHeavyData();

		let myRadios = document.getElementsByName('tabs2');
		let setCheck;
		let x = 0;
		for(x = 0; x < myRadios.length; x++){
			myRadios[x].onclick = function(){
				if(setCheck != this){
					setCheck = this;
				}else{
					this.checked = false;
					setCheck = null;
			}
			};
	}
	},[])

	{/*Snippet below is used to open a tab and close any others that may be open. Snippet from 'https://www.tailwindtoolbox.com/components/accordion'*/}
	
	const scrollToTopHandler = () =>{
		window.scroll({
			top: 100,
			left: 100,
			behavior: 'smooth'
		  })
	}
	return (
		<>
		<Header />
		<div className='container mx-auto p-4'>
			
			<div className='w-full md:w-9/12 md:mx-auto grid grid-cols-1 gap-4 md:grid-cols-2'>
				<h1 className='w-full text-center md:text-left pl-3 lg:pl-8 font-semibold my-auto  md:text-5xl lg:text-7xl'>Do you ever dream of what's out there?</h1>
				<img src='http://assets.stickpng.com/images/58e911aceb97430e819064d8.png' alt='rocket clipart picture' className='w-auto h-auto sm:mx-auto'/>
			</div>
			<div className='w-full  md:w-4/6 mx-auto mb-6 mt-3'>
				<p className='text-gray-500 text-center md:text-left font-medium mb-0 text-lg lg:text-2xl'>
					SpaceX is at the forefront of American space exploration, and students have access to a variety of data from past and ongoing missions.
					Check out the posts below for some quick bits about their most recent launches, as well as access to articles and video for each launch.
				</p>
			</div>

			<div className='w-full md:w-8/12 mx-auto p-4 bg-gradient-to-b from-gray-50 to-red-300 mb-8'>
				<div className='shadow-md h-auto overflow-auto bg-white'>
					{loaded ? (
						<div>
							{/*Base Accordion component attributed to https://www.tailwindtoolbox.com/components/accordion*/}
							{launches.data.slice(0,50).map(launch =>{
								return ( 
									<Accordion key={launch.flight_number}
										flightNumber={launch.flight_number}
										missionName={launch.mission_name}
										launchYear={launch.launch_year}
										patch={launch.links.mission_patch_small}
										rocketName={launch.rocket.rocket_name}
										rocketId={launch.rocket.rocket_id}
										launchDate={launch.launch_date_utc}
										launchSite={launch.launch_site.site_name_long}
										details={launch.details}
										article={launch.links.article_link}
										video={launch.links.video_link}
										falconNineWeight={falconNineData.data.mass.lb}
										falconNineCost={falconNineData.data.cost_per_launch}
										falconHeavyWeight={falconHeavyData.data.mass.lb}
										falconHeavyCost={falconHeavyData.data.cost_per_launch}
									/>	
								)
							})}
						</div>
					) : (<div className="spinner">
							<div className="rect1"></div>
							<div className="rect2"></div>
							<div className="rect3"></div>
							<div className="rect4"></div>
							<div className="rect5"></div>
						</div>
						)}
				</div>
			</div>
			<div className='fixed z-10 bottom-5 right-2'>
				<button className='mx-auto w-12 h-12 md:w10 bg-red-500 text-3xl rounded font-semibold text-white' 
					onClick={scrollToTopHandler}>
					^
				</button>
			</div>
		</div>
		</>
	)
}

export default IndexPage
