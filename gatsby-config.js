module.exports = {
	siteMetadata: {
		title: 'Gatsby Space'
	},
	plugins: [`gatsby-plugin-postcss`,`gatsby-transformer-sharp`, `gatsby-plugin-sharp`]
}
